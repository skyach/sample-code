class CarsController < ApplicationController
  require 'will_paginate/array'
  before_filter :authenticate_user!
  before_filter :rules_groups_permissions

  def index
    redirect_to root_path
  end

  def search
    set_order
    @cars = Car.find_by_location params[:car]
    params[:car][:pnr] = 0 unless params[:car][:pnr]
    session[:cars] = @cars
    session[:order] = {
        :pickup_location => params[:car][:pickup_location],
        :return_location => params[:car][:return_location],
        :pickup_date => params[:car][:pickup_date] + ' ' + params[:car][:pickup_time],
        :return_date => params[:car][:return_date] + ' ' + params[:car][:return_time],
        :pnr => params[:car][:pnr]
    }
    @cars
    @credit_card = current_user.car_credit_card
  end

  def book
    set_order
    user = User.find(params[:booker_id])
    car = session[:cars][params[:id].to_i]
    if car
      options = {
          :source => { :pseudo_city_code => 'SAV' },
          :pickup => {
              :date => session[:order][:pickup_date],
              :code => session[:order][:pickup_location],
              :extended_code => ((car.vendor_location)? (car.vendor_location['ExtendedLocationCode']) : ""),
              :counter => ((car.vendor_location)? (car.vendor_location['CounterLocation']) : "")
          },
          :return => {
              :date => session[:order][:return_date],
              :code => session[:order][:return_location],
              :extended_code => ((car.drop_off_location)? (car.drop_off_location['ExtendedLocationCode']) : ""),
              :counter => ((car.drop_off_location)? (car.drop_off_location['CounterLocation']) : "")
          },
          :customer => {
              :birth_date => '1970-01-01',
              :phone => '816-555-1111',
              :name => user.first_name,
              :surname => user.last_name
          },
          :vendor_code => car.vendor_code,
          :car => {
              :air_condition => car.air_condition,
              :transmission => car.transmission_type,
              :fuel_type => car.fuel_type,
              :category => car.category,
              :door_count => car.door_count,
              :size => car.class_size,
              :pnr => params[:car][:pnr]
          }
      }

      booking_type = (params[:car][:pnr].to_i == 0)? "car" : "hotel_and_car"

      if car = Car::book(options, current_user)
        car.update_attributes(:car_pickup_location => options[:pickup][:code],
                              :car_return_location => options[:return][:code],
                              :car_pickup_date => options[:pickup][:date].split(" ")[0],
                              :car_pickup_time => options[:pickup][:date].split(" ")[1],
                              :car_return_date => options[:return][:date].split(" ")[0],
                              :car_return_time => options[:return][:date].split(" ")[1],
                              :car_air_condition => options[:car][:air_condition],
                              :car_transmission => options[:car][:transmission],
                              :car_fuel_type => options[:car][:fuel_type],
                              :car_category => options[:car][:category],
                              :car_door_count => options[:car][:door_count],
                              :car_size => options[:car][:size],
                              :car_vendor_code => options[:vendor_code],
                              :booking_type => booking_type
        )
        #puts car.inspect
        #exit
        flash[:notice] = 'Car was booked'
        options[:car][:pnr] = car.reservation_id
        @car = options
        redirect_to car
      else
        flash[:alert] = 'Cannot book this car'
        redirect_to root_path
      end
    else
      flash[:alert] = 'Cannot find this car'
      redirect_to root_path
    end

  end

  def booking_details
    @booking = Booking.find_by_id(params[:booking_id])
  end



  private

  def set_order
    @url = request.url
    hash = { hotel: {}, car: {} }
    hash[:hotel] = params[:hotel].clone if params[:hotel].present?
    hash[:car]= params[:car].clone if params[:car].present?
    @order = hash
    session[ :car ] = params[ :car ] unless params[ :car ].blank?
    session[ :next_step ] = params[ :next_step ] unless params[ :next_step ].blank?
  end

  def rules_groups_permissions
    if request[:action].to_s == "index"
      check_for_permission(request[:action], :allow_book_car, :allow_administration_access)
    end
    if request[:action].to_s == "search"
      check_for_permission(request[:action], :allow_book_car, :allow_administration_access)
    end
    if request[:action].to_s == "book"
      check_for_permission(request[:action], :allow_book_car, :allow_administration_access)
    end
    if request[:action].to_s == "booking_details"
      check_for_permission(request[:action], :allow_book_car, :allow_administration_access)
    end
    if request[:action].to_s == "set_order"
      check_for_permission(request[:action], :allow_book_car, :allow_administration_access)
    end

  end
end