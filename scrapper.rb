require 'net/http'
require 'uri'
require 'openssl'
class GetKloutData
  #attr_accessor :company_identifier, :payloadID, :customerID, :sessionID

  #This method is a Constructor.
  #This method create the instance variables in the class, those can be used in all methods.
  #def initialize(company_identifier, payloadID, customerID, sessionID)
  #  self.company_identifier = company_identifier
  #  self.payloadID = payloadID
  #  self.customerID = customerID
  #  self.sessionID = sessionID
  #end

  #This method is called automatically once An instance of this class is created.
  #This method initiates all the further process in this class.
  #def perform
  #  get_company_profile(@company_identifier, @payloadID, @customerID)
  #end
  def perform
    if payload_record = Payload.find(:first)
      @payload_record_id = payload_record.id
      @company_identifier = payload_record.company_identifier
      @payloadID = payload_record.payload_id
      @customerID = payload_record.customer_id
      @sessionID = payload_record.session_id

      get_company_profile(@company_identifier, @payloadID, @customerID)
    end
  end


  #Instance of this method is called in every other method of this class.
  #This method invokes the klout with the given credentials.
  #API is defined in "config/initializers/klout_config.rb".
  def klout
    RockyKlout.new(API_KEY)
  end

  #This methods fires a Query on Klout API to get the required data.
  #If company does not exist in our database, it create the company with the data got from Klout.
  #If company already exists in our database, this method just returns the found object. It does not create a duplicate company to control redundancy.
  #This method further calls method 'get_company_topics' to extract list of topics
  def get_company_profile(raw_company_identifier, payload_id, customer_id)
    company_identifier = Array.new
    company_identifier << raw_company_identifier
    company_hash = Hash.new
    begin
      kl_companies = klout.user_show(company_identifier).parsed_response["users"]
    rescue Exception
      raise "User not found"
    end
    kl_companies.each do |kl_company|
      company_hash["company_identifier"] = company_identifier[0]
      company_hash["payload_id"] = payload_id
      company_hash["customer_id"] = customer_id
      company_hash["twitter_id"] = kl_company["twitter_id"]
      company_hash["twitter_screen_name"] = kl_company["twitter_screen_name"]
      company_hash["kscore"] = kl_company["score"]["kscore"]
      company_hash["slope"] = kl_company["score"]["slope"]
      company_hash["description"] = kl_company["score"]["description"]
      company_hash["kclass_id"] = kl_company["score"]["kclass_id"]
      company_hash["kclass"] = kl_company["score"]["kclass"]
      company_hash["kclass_description"] = kl_company["score"]["kclass_description"]
      company_hash["kscore_description"] = kl_company["score"]["kscore_description"]
      company_hash["network_score"] = kl_company["score"]["network_score"]
      company_hash["true_reach"] = kl_company["score"]["true_reach"]
      company_hash["delta_1day"] = kl_company["score"]["delta_1day"]
      company_hash["delta_5day"] = kl_company["score"]["delta_5day"]
      company_hash["amplification_score"] = kl_company["score"]["amplification_score"]
      company = Company.find_or_create_by_twitter_screen_name(company_hash)
      get_company_topics(company, company_identifier)
    end rescue Exception
  end

  #This methods fires a Query on Klout API to get the required data.
  #If Topics array does not exist in our database, it create the topic with the data got from Klout.
  #Topic hash is saved as a serialize data.
  #If topic already exists in our database, this method just returns the found object. It does not create a duplicate topic to control redundancy.
  #This method further calls method 'get_company_influencers' to extract list of influencers.
  def get_company_topics(company, company_identifier)
    kl_topics = klout.user_topics(company_identifier).parsed_response["users"] rescue Exception
    kl_topics.each do |topic|
      topic["company_identifier"] = company_identifier[0]
      company.topics.find_or_create_by_twitter_screen_name(topic)
    end rescue Exception
    get_company_influencers(company, company_identifier)
  end

  #This methods fires a Query on Klout API to get the required data.
  #If Influencers array does not exist in our database, it create the topic with the data got from Klout.
  #Topic hash is saved as a serialize data.
  #If Influencer array already exists in our database, this method just returns the found object. It does not create a duplicate Influencer to control redundancy.
  #This method further calls method 'get_company_influencees' to extract list of influencees.
  def get_company_influencers(company, company_identifier)
    kl_influencers = klout.relationship_influenced_by(company_identifier).parsed_response["users"][0]["influencers"]
    kl_influencers.each do |kl_influencer|
      kl_influencer["company_identifier"] = company_identifier[0]
      company.influencers.find_or_create_by_twitter_screen_name(kl_influencer)
    end
    get_company_influencees(company, company_identifier)
  end

  #This methods fires a Query on Klout API to get the required data.
  #If Influencees array does not exist in our database, it create the topic with the data got from Klout.
  #Topic hash is saved as a serialize data.
  #If Influencee array already exists in our database, this method just returns the found object. It does not create a duplicate Influencee to control redundancy.
  def get_company_influencees(company, company_identifier)
    kl_influencees = klout.relationship_influencer_of(company_identifier).parsed_response["users"][0]["influencers"]
    kl_influencees.each do |kl_influencee|
      kl_influencee["company_identifier"] = company_identifier[0]
      company.influencees.find_or_create_by_twitter_screen_name(kl_influencee)
    end
  end

  #This method is called if all the data is successfully fetched for a company.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/klout_config.rb".
  #This method posts the success status xml on DISTRIBUTOR_STATUS_URL on success.
  def success(job)
    #url = URI.parse(URI.encode(DISTRIBUTOR_STATUS_URL))
    #params = {'status[payload_id]' => @payloadID, 'status[customer_id]' => @customerID, 'status[company_identifier]' => @company_identifier, 'status[klout]' => 'success'}
    #res = Net::HTTP.post_form(url, params)
    Payload.destroy(@payload_record_id)
    if(Payload.all.count > 0)
      Delayed::Job.enqueue GetKloutData.new
    else
      work_status_url = URI.parse(DISTRIBUTOR_WORK_STATUS_URL)
      work_status_params = {'agent[name]' => 'klout_agent', 'agent[work_state]' => 'completed'}
      work_status_response = Net::HTTP.post_form(work_status_url, work_status_params)
    end
  end

  #This method is called if there is any error while the data extraction process from klout.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/klout_config.rb".
  #This method posts the failure status xml on DISTRIBUTOR_STATUS_URL on failure.
  def error(job, exception)
    Payload.destroy(@payload_record_id)
    if(Payload.all.count > 0)
      Delayed::Job.enqueue GetKloutData.new
    else
      work_status_url = URI.parse(DISTRIBUTOR_WORK_STATUS_URL)
      work_status_params = {'agent[name]' => 'klout_agent', 'agent[work_state]' => 'completed'}
      work_status_response = Net::HTTP.post_form(work_status_url, work_status_params)
    end
    url = URI.parse(URI.encode(DISTRIBUTOR_STATUS_URL))
    params = {'error[agent_name]' => 'Foursquare', 'error[username]' => @company_identifier, 'error[message]' => exception.to_s }
    res = Net::HTTP.post_form(url, params)
  end

  #This method is called if there is failure while the data extraction process from klout.
  #DISTRIBUTOR_STATUS_URL is defined in "config/initializers/klout_config.rb".
  #This method posts the failure status xml on DISTRIBUTOR_STATUS_URL on failure.
  def failure
  end

end